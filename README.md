# TrueNAS 监测栈配置

基于 Docker 的 TrueNAS Scale 监测栈配置，含 Docker Compose 部署脚本 `docker-compose.yml`、Telegraf 配置文件 `telegraf.conf` 和 Grafana 面板配置 `grafana.json`。配置详情请移步“什么值得买”社区：<https://post.smzdm.com/p/a4x9vo6l/>
